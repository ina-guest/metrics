import argparse
import gitlab
import logging
import requests
import sqlite3
import time
import os

from datetime import datetime
from flask import Flask, request
from influxdb import InfluxDBClient
from multiprocessing import Process

import config

app = Flask(__name__)
logger = logging.getLogger()

session = requests.Session()
session.headers.update({'User-Agent': 'Salsa-CI Metrics Bot (prittiau.debian.net; inaki@malerba.space)'})

SALSA = gitlab.Gitlab(
        'https://salsa.debian.org/',
        private_token=config.GITLAB_TOKEN,
        session=session,
        )

class Poller():
    def __init__(self):
        self.db_path = config.SQLITE_DB_PATH
        self.db_isolation = config.SQLITE_DB_ISOLATION
        self.influx = InfluxDBClient(config.INFLUX_HOST, config.INFLUX_PORT, config.INFLUX_USER, config.INFLUX_PORT, config.INFLUX_DB)

    def _db_connect(self):
        self.conn = sqlite3.connect(self.db_path, isolation_level=self.db_isolation)
        self.conn.execute(config.SQL_CREATE_TABLE)

    def _db_close(self):
        self.conn.close()

    def __enter__(self):
        self._db_connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._db_close()

    def new_pipeline(self, project_id, pipeline_id):
        logger.info("Creating %s:%s", project_id, pipeline_id)
        try:
            self.conn.execute(config.SQL_INSERT_PIPELINE, (project_id, pipeline_id))
        except sqlite3.IntegrityError:
            logger.warning("Already exists")

    def poll_all(self):
        logger.debug("Polling all pipelines")

        pipelines = self.conn.execute(config.SQL_SELECT_PIPELINES_PENDING).fetchall()
        logging.debug("Found %d results", len(pipelines))

        for project_id, pipeline_id in pipelines:
            try:
                self.poll_project(project_id, pipeline_id)
            except Exception as e:
                logger.exception(e)

    def poll_project(self, project_id, pipeline_id):
        logger.debug("Polling %s:%s", project_id, pipeline_id)

        try:
            project = SALSA.projects.get(project_id)
            pipeline = project.pipelines.get(pipeline_id)
        except:
            '''
            a_ Project can be private.
            b_ Project can be public but the pipelines not.
            '''
            self.conn.execute(config.SQL_UPDATE_PIPELINE_MARK_NOT_PENDING, (project_id, pipeline_id))
            return

        jobs = pipeline.jobs.list()

        if pipeline.attributes['status'] not in config.JOB_STATUS_FINISHED:
            return

        logger.info("Pushing %s:%s to influx (%s)", project_id, pipeline_id, pipeline.attributes['web_url'])
        self.post_influx(pipeline, jobs)

        self.conn.execute(config.SQL_UPDATE_PIPELINE_MARK_NOT_PENDING, (project_id, pipeline_id))

    def post_influx(self, pipeline, jobs):
        json_body = []
        for job in jobs:
            json_body.append(
                {
                    "measurement": config.INFLUX_MEASUREMENT_JOBS,
                    "tags": {
                        'runner_id': job.attributes['runner']['id'] if jobs.attributes['runner'] else 0,
                        'job_id': job.attributes['id'],
                        'job_stage': job.attributes['stage'],
                        'job_name': job.attributes['name'],
                        'job_status': job.attributes['status'],
                    },
                    "time": pipeline.attributes['created_at'],
                    "fields": {
                        'project_id': pipeline.attributes['project_id'],
                        'job_status': config.JOB_STATUS_INT[job.attributes['status']],
                        'job_duration': job.attributes['duration'],
                        'pipeline_id': pipeline.attributes['id'],
                        'pipeline_status': config.JOB_STATUS_INT[pipeline.attributes['status']],
                        'pipeline_duration': pipeline.attributes['duration'],
                    },
                }
            )
        json_body.append(
            {
                "measurement": config.INFLUX_MEASUREMENT_PIPELINES,
                "tags": {
                    'pipeline_status': pipeline.attributes['status'],
                    'pipeline_id': pipeline.attributes['id'],
                },
                "time": pipeline.attributes['created_at'],
                "fields": {
                    'project_id': pipeline.attributes['project_id'],
                    'job_amount': len(jobs),
                    'pipeline_status': config.JOB_STATUS_INT[pipeline.attributes['status']],
                    'pipeline_duration': pipeline.attributes['duration'],
                },
            }
        )
        self.influx.write_points(json_body)


@app.route('/post', methods=['POST'])
def endpoint():
    project_id = request.json.get('project_id')
    pipeline_id = request.json.get('pipeline_id')

    with Poller() as p:
        p.new_pipeline(project_id, pipeline_id)

    return "OK"


def configure_logger():
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)-5s - %(message)s"
    )
    level = os.getenv('LOG_LEVEL', "INFO")
    logger.setLevel(level)


def run_loop():
    with Poller() as p:
        while True:
            p.poll_all()
            time.sleep(config.LOOP_EVERY_S)


if __name__ == "__main__":
    configure_logger()

    p = Process(target=run_loop)
    p.start()
    app.run(debug=True, use_reloader=False)
    p.join()
